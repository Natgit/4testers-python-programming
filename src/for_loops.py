def print_students_first_name_capitalized(list_of_students_first_names):
    for first_name in list_of_students_first_names:
        print(first_name.capitalize())


# 2
def print_first_ten_integers_squared():
    for integer in range(1, 31):
        print(integer ** 2)


if __name__ == '__main__':
    list_of_students = ["gIenia", "franek", "KLEopatra", "aldona", "kATE"]
    print_students_first_name_capitalized(list_of_students)
    print_first_ten_integers_squared()
