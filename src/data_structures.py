# 1
movies = ['Transformers1', 'Transformers2', 'Transformers3', 'Transformers4', 'Transformers5']

last_movie = movies[-1]
movies.append("Predator")
movies.append('Terminator')
print(len(movies))
middle_movies = movies[2:5]
print(middle_movies)

movies.insert(0, 'Top Gun')
print(movies)

# 2

emails = ['a@example.com', 'b@example.com']
print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@example.com')

friend = {
    'first_name' : 'Anna',
    'age' : 35,
    'friend_hobby' : ['swimming', 'singing']
}

friend_hobbies = friend['hobby']
print('Hobbies of my friend:', friend_hobbies)
print(f"Number of {len(friend_hobbies)}")

friend['married'] = True
friend['age'] = 34
