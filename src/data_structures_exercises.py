# 2
import string
import random


# 1
def calculate_average_of_list_numbers(input_list):
    return sum(input_list) / len(input_list)


def calculate_average_value(x, y):
    return (x + y) / 2


# 2
# Get random password pf length 8 with letters, digits, and symbols
def generate_random_password():
    characters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(characters) for i in range(8))


# Function generating random login data
def generate_login_data(email_address):
    generated_password = generate_random_password()
    return {"email": email_address, "password": generated_password}


# 3
def calculate_warrior_level(exp_points):
    return exp_points // 1000
    # return int(exp_points / 1000)

def describe_the_warrior(warrior_description):
    nick = warrior_description["nick"]
    type = warrior_description["type"]
    exp_points = warrior_description["exp_points"]
    level = calculate_warrior_level(exp_points)

    return f"The player {nick} is of type {type} and has {exp_points} EXP. Player is on level {level}"


if __name__ == '__main__':
    print(generate_login_data("andor@starwars.com"))
    print(generate_login_data("mando@starwars.com"))
    print(generate_login_data("kenobi@starwars.com"))

    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    january_average = calculate_average_of_list_numbers(january)
    february_average = calculate_average_of_list_numbers(february)
    bimonthly_average = calculate_average_value(january_average, february_average)
    print(bimonthly_average)

    player_1 = {
        "nick": "Maestro",
        "type": "warrior",
        "exp_points": 3000,
    }

    player_2 = {
        "nick": "Prime",
        "type": "defender",
        "exp_points": 6000,
    }

    print(describe_the_warrior(player_1))
    print(describe_the_warrior(player_2))
