addresses = [
    {'city': 'warsaw', 'street': 'poselska', 'house_number': 54, 'post_code': '00-300'},
    {'city': 'cracow', 'street': 'senatorska', 'house_number': 3, 'post_code': '31-200'},
    {'city': 'wroclaw', 'street': 'akademicka', 'house_number': 169, 'post_code': '62-400'},
]

if __name__ == '__main__':
    print(addresses[-1]['post_code'])
    print(addresses[1]['city'])
    addresses[0]['street'] = 'aleje_jerozolimskie'
    print(addresses)
