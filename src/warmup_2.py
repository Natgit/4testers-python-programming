# 1
def calculate_square_of_number(number):
    return number ** 2


# 2
def convert_temperature_in_celsius_to_fahrenheit(temperature_in_celsius):
    return temperature_in_celsius * 1.8 + 32


# 3
def calculate_volume_of_cuboid(a, b, c):
    return a * b * c


if __name__ == '__main__':
    # 1
    print(calculate_square_of_number(0))
    print(calculate_square_of_number(16))
    print(calculate_square_of_number(2.55))
    # 2
    print(convert_temperature_in_celsius_to_fahrenheit(20))
    # 3
    print(calculate_volume_of_cuboid(3, 5, 7))

