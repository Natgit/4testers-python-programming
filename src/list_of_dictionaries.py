animals = [
{
              'type': 'dog',
              'age': 2,
              'female': True

          },
{
    'type': 'cat',
    'age': 1,
    'female': False
},
{
    'type': 'fish',
    'age': 1,
    'female': True
}
    ]

print(len(animals))
print(animals[0])
print(animals[-1])
last_animal = animals[-1]
last_animal_age = last_animal['age']
print('The last animal age is equal', last_animal_age)
print(animals[-1]['female'])
animals.append(
    {
        'type': "zebra",
        'age': 2,
        'female': True
    }
)
print(animals)
