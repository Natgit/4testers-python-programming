first_name = "Natalia"
last_name = "N"
age = 34

print(first_name)
print(last_name)
print(age)

# definition of variables for description of a friend and friendship

name_of_friend = "Anna"
age_of_friend = 34
number_of_friend_pets = 1
drivers_license_of_friend = True
friendship_duration_in_years = 10.5

print(name_of_friend, age_of_friend, number_of_friend_pets, drivers_license_of_friend, friendship_duration_in_years, sep=', ')

