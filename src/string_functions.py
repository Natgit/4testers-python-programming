# 1
def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Witaj {person_name}! Miło Cię widzieć w naszym mieście: {city}!")

# 2
def generate_email_addresses(first_name, last_name):
    print(f"{first_name.lower()}.{last_name.lower()}@4testers.pl")


if __name__ == '__main__':
    #1
    print_greetings_for_a_person_in_the_city("Kasia", "Szczecin")
    print_greetings_for_a_person_in_the_city("Ala", "Wrocław")

    #2
    generate_email_addresses("Janusz", "Nowak")
    generate_email_addresses("Korneliusz", "Padaka")
