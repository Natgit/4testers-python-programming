import random
import datetime

female_names = ['Karen', 'Jane', 'Natalia', 'Anna', 'Hannah', 'Mia', 'Judy', 'Virginie', 'Adeline', 'Jennifer', 'Lisa',
                'Lindsay', 'Lesley']
male_names = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin', 'Neo', 'Walt']
last_names = ['Smith', 'Anderson', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'Canada', 'USA', 'Czech Republic', 'Germany', 'Spain', 'France', 'Portugal', 'UK', 'Slovakia',
             'Denmark', 'Norway', 'Sweden', 'other']


def get_a_random_female_name():
    return random.choice(female_names)


def get_a_random_male_name():
    return random.choice(male_names)


def get_a_random_last_name():
    return random.choice(last_names)


def get_a_random_country():
    return random.choice(countries)


def get_a_random_email(first_name, last_name):
    return first_name.lower() + '.' + last_name.lower() + '@example.com'


def get_a_random_age():
    return random.randint(5, 45)


def get_if_adult(random_age):
    if random_age >= 18:
        return True
    else:
        return False


def calculate_a_year_of_birth(random_age):
    # date = datetime.date.today()
    # year = int(date.strftime("%Y"))
    date = datetime.date.today()
    year = date.year
    return year - random_age


def generate_random_dictionary_female():
    random_female_first_name = get_a_random_female_name()
    random_last_name = get_a_random_last_name()
    random_country = get_a_random_country()
    random_email = get_a_random_email(random_female_first_name, random_last_name)
    random_age = get_a_random_age()
    adult_boolean = get_if_adult(random_age)
    birth_year = calculate_a_year_of_birth(random_age)

    return {
        'firstname': random_female_first_name,
        'lastname': random_last_name,
        'country': random_country,
        'email': random_email,
        'age': random_age,
        'adult': adult_boolean,
        'birth_year': birth_year,
    }


def generate_random_dictionary_male():
    random_male_first_name = get_a_random_male_name()
    random_last_name = get_a_random_last_name()
    random_country = get_a_random_country()
    random_email = get_a_random_email(random_male_first_name, random_last_name)
    random_age = get_a_random_age()
    adult_boolean = get_if_adult(random_age)
    birth_year = calculate_a_year_of_birth(random_age)

    return {
        'firstname': random_male_first_name,
        'lastname': random_last_name,
        'country': random_country,
        'email': random_email,
        'age': random_age,
        'adult': adult_boolean,
        'birth_year': birth_year,
    }

def generate_list_of_dictionaries_female_and_male(number_of_dictionaries):
    list_of_dictionaries = []
    for dictionary in range(number_of_dictionaries // 2):
        list_of_dictionaries.append(generate_random_dictionary_female())
    for dictionary in range(number_of_dictionaries // 2):
        list_of_dictionaries.append(generate_random_dictionary_male())
    return list_of_dictionaries


if __name__ == '__main__':
    # print(generate_random_dictionary_female())
    print(generate_random_dictionary_male())
    for dictionary in generate_list_of_dictionaries_female_and_male(10):
        print(f"Hi! I'm {dictionary['firstname']} {dictionary['lastname']}. I come from {dictionary['country']} and I was born in {dictionary['birth_year']}.")
