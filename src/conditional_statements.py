def print_temperature_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:
        print("It's getting hot")
    elif temperature_in_celsius > 0:
        print("It's quite OK.")
    else:
        print("It's cold")


def print_if_person_adult(age_person):
    if age_person >= 18:
        return True
    else:
        return False
    # return True if age_person >= 18 else False

if __name__ == '__main__':
    temperature_in_celsius = 0
    print_temperature_description(temperature_in_celsius)

    age_ania = 3
    age_jan = 18
    age_kali = 21
    print(print_if_person_adult(age_ania))
    print(print_if_person_adult(age_jan))
    print(print_if_person_adult(age_kali))

