from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(0,40):
        print('Hello!', i)



#new
def print_square_roots_of_numbers(list_of_numbers):
    for number in list_of_numbers:
        print(sqrt(number))

# new
def calculate_temperature_from_celsius_to_fahrenheit(insert_list_of_temperatures):
    list_of_temperatures_in_fahrenheit = []
    for temperature in insert_list_of_temperatures:
        temperature = temperature * 9/5 + 32
        list_of_temperatures_in_fahrenheit.append(temperature)
    return list_of_temperatures_in_fahrenheit



if __name__ == '__main__':

    list_of_measurement_results = [11.0, 123, 69, 80, 43, 23, 1, 5, 77]
    print_square_roots_of_numbers(list_of_measurement_results)

    list_of_temperatures = [10.3, 23.4, 15.8, 19, 14, 23, 25, 0, -18]
    print(calculate_temperature_from_celsius_to_fahrenheit(list_of_temperatures))
