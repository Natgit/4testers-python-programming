# Nowo utworzony samochód ma przebieg 0
# Samochód po przejechaniu 100 ma przebieg 100
# Samochód po przejechaniu 100, a następnie 200 ma przebieg 300
# Samochód z roku 2020 z przebiegiem 119 tys. ma nadal gwarancję
# Samochód z roku 2007 z przebiegiem 19 tys. nie ma gwarancji
# Opis Samochodu “Mazda CX-3” z roku 2001 z przebiegiem 230 tys. to
# “This is a Mazda CX-3 made in 2001. Currently it drove 230000 kilometers”
