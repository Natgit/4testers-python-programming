from src.conditional_statements import print_if_person_adult


def test_print_if_person_under_adult():
    test_adult_age = print_if_person_adult(17)
    assert test_adult_age == False

def test_print_if_person_adult():
    test_adult_age = print_if_person_adult(18)
    assert test_adult_age == True

def test_print_if_person_over_adult_age():
    test_adult_age = print_if_person_adult(19)
    assert test_adult_age == True

